package server

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"

	"git.sipher.co/sproutways/external/shared/configs"

	"git.sipher.co/sproutways/external/api/authentication/controllers"
)

// API : _ => http.Handler
func API() http.Handler {
	time.Local = time.UTC
	//gin.SetMode(gin.ReleaseMode)

	// gin
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, POST",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     false,
		ValidateHeaders: false,
	}))

	// v1
	v1 := router.Group("/v1")

	v1.POST("/create-account", controllers.PostCreateAccount)
	v1.POST("/sign-in", controllers.PostSignIn)
	v1.POST("/username/unique", controllers.PostUsernamelUnique)
	v1.POST("/email/unique", controllers.PostEmailUnique)
	v1.POST("/password/forgot", controllers.PostPasswordForgot)
	v1.POST("/password/change", controllers.PostPasswordChange)

	return router
}

// Server : &http.Server
var Server = &http.Server{
	Addr:         configs.GetServer().APIAuthentication.Port,
	Handler:      API(),
	ReadTimeout:  5 * time.Second,
	WriteTimeout: 10 * time.Second,
}
