package main

import (
	"time"

	"github.com/gin-gonic/gin"
	"github.com/itsjamie/gin-cors"

	"git.sipher.co/sproutways/external/shared/configs"
	"git.sipher.co/sproutways/external/shared/services/helpers"

	"git.sipher.co/sproutways/external/api/authentication/controllers"
)

func main() {
	time.Local = time.UTC
	//gin.SetMode(gin.ReleaseMode)

	// gin
	router := gin.New()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(cors.Middleware(cors.Config{
		Origins:         "*",
		Methods:         "GET, POST",
		RequestHeaders:  "Origin, Authorization, Content-Type",
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     false,
		ValidateHeaders: false,
	}))

	v1 := router.Group("/v1")

	v1.POST("/create-account", controllers.PostCreateAccount)
	v1.POST("/sign-in", controllers.PostSignIn)
	v1.POST("/username/unique", controllers.PostUsernamelUnique)
	v1.POST("/email/unique", controllers.PostEmailUnique)
	v1.POST("/password/forgot", controllers.PostPasswordForgot)
	v1.POST("/password/change", controllers.PostPasswordChange)

	// run
	err := router.Run(configs.GetServer().APIAuthentication.Port)

	if err != nil {
		helpers.Fatal(err)
	}
}
