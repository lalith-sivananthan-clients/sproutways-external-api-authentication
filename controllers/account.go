package controllers

import (
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/leebenson/conform"
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/authentications"
	"git.sipher.co/sproutways/external/shared/services/codes"
	"git.sipher.co/sproutways/external/shared/services/helpers"
	"git.sipher.co/sproutways/external/shared/services/validations"

	"git.sipher.co/sproutways/external/daemon/queue/dispatchers"
)

type (
	requestPostAccountCreate struct {
		Username             string `form:"Username"        conform:"trim,lower"       binding:"required" validate:"required,min=3,max=255,username,usernameUnique"`
		Email                string `form:"Email"           conform:"trim,lower,email" binding:"required" validate:"required,email,emailUnique"`
		Password             string `form:"Password"        conform:"trim"             binding:"required" validate:"required,min=6,eqfield=PasswordConfirmation"`
		PasswordConfirmation string `form:"PasswordConfirm" conform:"trim"             binding:"required" validate:"required,min=6"`
	}

	requestPostAccountSignIn struct {
		Email    string `form:"Email"    conform:"trim,lower,email" binding:"required" validate:"required,email"`
		Password string `form:"Password" conform:"trim"             binding:"required" validate:"required"`
	}

	requestPostAccountEmailUnique struct {
		Email string `form:"Email" conform:"trim,lower,email" binding:"required" validate:"required,email,emailUnique"`
	}

	requestPostAccountUsernameUnique struct {
		Username string `form:"Username" conform:"trim,lower" binding:"required" validate:"required,min=3,max=255,username,usernameUnique"`
	}

	requestPostAccountPasswordForgot struct {
		Email string `form:"Email" conform:"trim,lower,email" binding:"required" validate:"required,email"`
	}

	requestPostAccountPasswordChange struct {
		Token           string `form:"Token"           conform:"trim" binding:"required" validate:"required,tokenPassword"`
		Password        string `form:"Password"        conform:"trim" binding:"required" validate:"required,min=6,eqfield=PasswordConfirm"`
		PasswordConfirm string `form:"PasswordConfirm" conform:"trim" binding:"required" validate:"required,min=6"`
	}
)

// POST -> v#/create-account
func PostCreateAccount(c *gin.Context) {
	// parameters -> bind
	var params requestPostAccountCreate

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// db : account -> data
	accountData := &models.Account{
		ID:          bson.NewObjectId(),
		Username:    params.Username,
		Email:       params.Email,
		Password:    authentications.CreatePassword(params.Password),
		PasswordSet: true,
		Confirmed:   false,
		Status:      "active",
		Connections: models.AccountConnections{
			Facebook: models.AccountConnectionsFacebook{
				Connected: false,
			},
			Google: models.AccountConnectionsGoogle{
				Connected: false,
			},
			LinkedIn: models.AccountConnectionsLinkedIn{
				Connected: false,
			},
		},
		Profile: models.AccountProfile{
			Avatar: models.MediaImageRef{
				MediaID: "",
				Status:  "notset",
			},
			NameFirst: "",
			NameLast:  "",
			Headline:  "",
			Story:     "",
		},
		When: models.WhenNew(),
	}

	err, code := CreateAccount(accountData)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, code))
		c.Abort()
		return
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST -> v#/sign-in
func PostSignIn(c *gin.Context) {
	// parameters -> bind
	var params requestPostAccountSignIn

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()

	// db : account -> query
	query := bson.M{
		"em": params.Email,
	}

	// db : account -> get
	account := &models.Account{}

	err = models.DBAuthenticationCollectionAccounts.Find(query).One(&account)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountGet))
		c.Abort()
		return
	}

	// authentication -> check
	authenticatePassword := authentications.Check(account.Password, params.Password)

	if authenticatePassword != true {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorAccountInvalidPassword))
		c.Abort()
		return
	}

	// authentication -> sign
	token, err := authentications.Sign(*account)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorJWTCreate))
		c.Abort()
		return
	}

	c.JSON(200, Response(true, codes.ResultSuccess, token))
}

// POST -> v#/email/unique
func PostEmailUnique(c *gin.Context) {
	// parameters -> bind
	var params requestPostAccountEmailUnique

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, false))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, false))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultSuccess, false))
		c.Abort()
		return
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST -> v#/username/unique
func PostUsernamelUnique(c *gin.Context) {
	// parameters -> bind
	var params requestPostAccountUsernameUnique

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, false))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, false))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultSuccess, false))
		c.Abort()
		return
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST -> v#/password/forgot
func PostPasswordForgot(c *gin.Context) {
	// parameters -> bind
	var params requestPostAccountPasswordForgot

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	// db : account || account-password -> refresh
	models.DBAuthenticationSessionAccounts.Refresh()
	models.DBAuthenticationSessionAccountPasswords.Refresh()

	// db : account -> query
	query := bson.M{
		"em": params.Email,
	}

	// db : account -> get
	account := &models.Account{}

	err = models.DBAuthenticationCollectionAccounts.Find(query).One(&account)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountGet))
		c.Abort()
		return
	}

	// db : account-password -> query
	query = bson.M{
		"_aid": account.ID,
	}

	// db : account-password : get
	accountPassword := &models.AccountPassword{}

	count, err := models.DBAuthenticationCollectionAccountPasswords.Find(query).Count()
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountPasswordGet))
		c.Abort()
		return
	}

	if count == 0 {
		// db : account-password -> data
		accountPassword = &models.AccountPassword{
			ID:        bson.NewObjectId(),
			AccountID: account.ID,
			Token:     helpers.CreateToken(64),
			When:      models.WhenTokenNew(),
		}

		// db : account-password -> create
		err = models.DBAuthenticationCollectionAccountPasswords.Insert(&accountPassword)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountPasswordCreate))
			c.Abort()
			return
		}
	} else {
		// db : account-password -> data
		update := bson.M{
			"$set": bson.M{
				"wh.ea": time.Now().Add(time.Minute * 60 * 24 * 7),
				"wh.ua": time.Now(),
			},
		}

		// db : account-password -> update
		err = models.DBAuthenticationCollectionAccountPasswords.Update(query, update)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountPasswordUpdate))
			c.Abort()
			return
		}

		// db : account-password -> get
		err = models.DBAuthenticationCollectionAccountPasswords.Find(query).One(&accountPassword)
		if err != nil {
			c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountPasswordGet))
			c.Abort()
			return
		}
	}

	// queue -> email
	data := make([]interface{}, 2)
	data[0] = *account
	data[1] = *accountPassword

	payload := &dispatchers.PayloadEmail{
		Template: dispatchers.TemplateAccountPasswordForgot,
		Data:     data,
	}

	dispatchers.Email(payload)

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}

// POST -> v#/password/change
func PostPasswordChange(c *gin.Context) {
	// parameters -> bind
	var params requestPostAccountPasswordChange

	err := c.Bind(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsBind))
		c.Abort()
		return
	}

	// parameters -> clean
	err = conform.Strings(&params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsConform))
		c.Abort()
		return
	}

	// parameters -> validate
	err = validations.Validate.Struct(params)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorParamsInvalid))
		c.Abort()
		return
	}

	token := strings.Split(params.Token, "-0-")
	accountID := bson.ObjectIdHex(token[0])
	accountPasswordID := bson.ObjectIdHex(token[1])

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()
	models.DBAuthenticationSessionAccountPasswords.Refresh()

	// db : account -> query
	query := bson.M{
		"_id": accountID,
	}

	// db : account -> data
	update := bson.M{
		"$set": bson.M{
			"pw":    authentications.CreatePassword(params.Password),
			"ps":    true,
			"wh.ua": time.Now(),
		},
	}

	// db : account -> update
	err = models.DBAuthenticationCollectionAccounts.Update(query, update)
	if err != nil {
		c.JSON(200, Response(true, codes.ResultError, codes.ErrorModelAccountUpdate))
		c.Abort()
		return
	}

	// db : account-password -> delete
	err = models.DBAuthenticationCollectionAccountPasswords.RemoveId(accountPasswordID)
	if err != nil {
		// TODO
		// queue -> db : account-password
		// delete : id -> accountPasswordID
	}

	account := &models.Account{}

	err = models.DBAuthenticationCollectionAccounts.FindId(accountID).One(&account)
	if err == nil {
		// queue -> email
		data := make([]interface{}, 1)
		data[0] = *account

		payload := &dispatchers.PayloadEmail{
			Template: dispatchers.TemplateAccountPasswordChanged,
			Data:     data,
		}

		dispatchers.Email(payload)
	}

	c.JSON(200, Response(true, codes.ResultSuccess, true))
}
