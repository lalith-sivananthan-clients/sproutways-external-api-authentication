package controllers

import (
	"gopkg.in/mgo.v2/bson"

	"git.sipher.co/sproutways/external/shared/models"
	"git.sipher.co/sproutways/external/shared/services/codes"
	"git.sipher.co/sproutways/external/shared/services/helpers"

	"git.sipher.co/sproutways/external/daemon/queue/dispatchers"
)

type (
	ResponseStruct struct {
		Success bool         `json:"Success"`
		Result  ResultStruct `json:"Result"`
	}

	ResultStruct struct {
		Code uint        `json:"Code"`
		Data interface{} `json:"Data"`
	}
)

func Response(success bool, code uint, data interface{}) (response ResponseStruct) {

	response.Success = success
	response.Result.Code = code
	response.Result.Data = data

	return
}

func CreateAccount(accountData *models.Account) (err error, code float32) {

	// db : refresh
	models.DBAuthenticationSessionAccounts.Refresh()
	models.DBAuthenticationSessionAccountEmails.Refresh()

	// db : account -> create
	err = models.DBAuthenticationCollectionAccounts.Insert(accountData)

	if err != nil {
		code = codes.ErrorModelAccountCreate
		return
	}

	// db : account-email -> data
	accountEmailData := &models.AccountEmail{
		ID:        bson.NewObjectId(),
		AccountID: accountData.ID,
		Email:     accountData.Email,
		Token:     helpers.CreateToken(64),
		When:      models.WhenTokenNew(),
	}

	// db : account-email -> create
	err = models.DBAuthenticationCollectionAccountEmails.Insert(accountEmailData)

	if err != nil {
		// TODO
		// queue -> db : account-email
		// create : accountEmailData
	} else {
		// queue -> email
		data := make([]interface{}, 2)
		data[0] = *accountData
		data[1] = *accountEmailData

		payload := &dispatchers.PayloadEmail{
			Template: dispatchers.TemplateAccountWelcome,
			Data:     data,
		}

		dispatchers.Email(payload)
	}

	return
}
